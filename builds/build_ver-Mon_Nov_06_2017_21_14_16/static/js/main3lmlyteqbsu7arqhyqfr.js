'use strict';

$(function () {
    $('.house_number:not(.disable)').click(function () {
        $('[house_wrapper]').removeClass('active');

        //        $(this).addClass('active');
        var nowHouse = $(this).attr('data-number');
        var activeHouse = $('[house_wrapper=' + nowHouse + ']');
        $(activeHouse).addClass('active');

        //        console.log(activeHouse);
    });
    $(document).ready(function () {
        var target = $('.include_info_wrapper .active').attr('data-target');
        //        console.log(target)
        $('#' + target).addClass('clicked');
    });

    $('.appartments').mouseover(function (e) {
        var $this = $(this);
        if (!$this.hasClass('clicked')) {
            $this.addClass('selected');
            var data = $this.attr('data-smalltext');
            //            console.log(data);
            $('<div class="short_desc">' + data + '</div>').appendTo('#svg-main');
            //            var elem_height = $('.short_desc').innerHeight(),
            //                elem_width = $('.short_desc').innerWidth();
        }
    }).mouseleave(function () {
        $(this).removeClass('selected');
        $('.short_desc').remove();
    }).mousemove(function (e) {
        var mouseX = e.pageX,
            //X coordinates of mouse
        mouseY = e.pageY,
            //Y coordinates of mouse
        coord = $(this)[0].getBoundingClientRect(),
            coord2 = $('.svg-plan')[0].getBoundingClientRect(),
            resultCoord = coord.x - coord2.x;
        //        console.log(mouseX + ' ' + mouseY)
        //        console.log(resultCoord)
        $('.short_desc').css({
            top: mouseY - 100,
            //            left: mouseX - ($('.short_desc').width()/2) - 1140
            left: resultCoord - 130
        });
    }).on('click', function (e) {
        $('.appartments').removeClass('clicked');

        $('.include_info_wrapper .include_info').removeClass('active');

        var $thisElement = $(this);
        $thisElement.addClass('clicked');
        $('.short_desc').remove();

        var id = $thisElement.attr('id');
        var include_info = $('.include_info[data-target=' + id + ']');
        $(include_info).addClass('active');

        //        var heightMT = $('.include_info.active .room-info').innerHeight;
        ////            doc_w = $(document).width();
        //        if(window.innerWidth > 992){
        //            console.log(heightMT);
        //            $('.house_trigger .inner').css('margin-top', -heightMT);
        //        }
    });

    $(document).ready(function () {
        var target2 = $('.include_info_wrapper2 .active').attr('data-target');
        //        console.log(target2)
        $('#' + target2).addClass('clicked');
    });

    $('.appartments2').mouseover(function (e) {
        var $this2 = $(this);
        if (!$this2.hasClass('clicked')) {
            $this2.addClass('selected');
            var data2 = $this2.attr('data-smalltext2');
            //            console.log(data);
            $('<div class="short_desc">' + data2 + '</div>').appendTo('#svg-main2');
            //            var elem_height = $('.short_desc').innerHeight(),
            //                elem_width = $('.short_desc').innerWidth();
        }
    }).mouseleave(function () {
        $(this).removeClass('selected');
        $('.short_desc').remove();
    }).mousemove(function (e) {
        var mouseX = e.pageX,
            //X coordinates of mouse
        mouseY = e.pageY,
            //Y coordinates of mouse
        coord = $(this)[0].getBoundingClientRect(),
            coord2 = $('.svg-plan2')[0].getBoundingClientRect(),
            resultCoord = coord.x - coord2.x;
        //        console.log(mouseX + ' ' + mouseY)
        //        console.log(resultCoord)
        $('.short_desc').css({
            top: mouseY - 100,
            //            left: mouseX - ($('.short_desc').width()/2) - 1140
            left: resultCoord - 130
            //            left: mouseX - coord2
        });
    }).on('click', function (e) {
        $('.appartments2').removeClass('clicked');

        $('.include_info_wrapper2 .include_info').removeClass('active');

        var $thisElement = $(this);
        $thisElement.addClass('clicked');
        $('.short_desc').remove();

        var id = $thisElement.attr('id');
        var include_info = $('.include_info_wrapper2  .include_info[data-target=' + id + ']');
        $(include_info).addClass('active');
    });
});
"use strict";
"use strict";
//
////var regions=[
////    {
////        "region_name": "Lombardia",
////        "region_code": "lom",
////        "population": 9794525
////    },
////    {
////        "region_name": "Campania",
////        "region_code": "cam",
////        "population": 5769750
////    },
////    {
////        "region_name": "Lazio",
////        "region_code": "laz",
////        "population": 5557276
////    },
////    {
////        "region_name": "Sicilia",
////        "region_code": "sic",
////        "population": 4999932
////    },
////    {
////        "region_name": "Veneto",
////        "region_code": "ven",
////        "population": 4881756
////    },
////    {
////        "region_name": "Emilia-Romagna",
////        "region_code": "emi",
////        "population": 4377487
////    },
////    {
////        "region_name": "Piemonte",
////        "region_code": "pie",
////        "population": 4374052
////    },
////    {
////        "region_name": "Puglia",
////        "region_code": "pug",
////        "population": 4050803
////    },
////    {
////        "region_name": "Toscana",
////        "region_code": "tos",
////        "population": 3692828
////    },
////    {
////        "region_name": "Calabria",
////        "region_code": "cal",
////        "population": 1958238
////    },
////    {
////        "region_name": "Sardegna",
////        "region_code": "sar",
////        "population": 1640379
////    },
////    {
////        "region_name": "Liguria",
////        "region_code": "lig",
////        "population": 1565127
////    },
////    {
////        "region_name": "Marche",
////        "region_code": "mar",
////        "population": 1545155
////    },
////    {
////        "region_name": "Abruzzo",
////        "region_code": "abr",
////        "population": 1312507
////    },
////    {
////        "region_name": "Friuli-Venezia Giulia",
////        "region_code": "fri",
////        "population": 1221860
////    },
////    {
////        "region_name": "Trentino-Alto Adige",
////        "region_code": "tre",
////        "population": 1039934
////    },
////    {
////        "region_name": "Umbria",
////        "region_code": "umb",
////        "population": 886239
////    },
////    {
////        "region_name": "Basilicata",
////        "region_code": "bas",
////        "population": 576194
////    },
////    {
////        "region_name": "Molise",
////        "region_code": "mol",
////        "population": 313341
////    },
////    {
////        "region_name": "Val d'Aosta",
////        "region_code": "vao",
////        "population": 127844
////    }
////];
////
////
////var temp_array= regions.map(function(item){
////    return item.population;
////});
////var highest_value = Math.max.apply(Math, temp_array);
//
//
//$(function() {
////    var i;
////    for(i = 0; i < regions.length; i++) {
////
////        $('#'+ regions[i].region_code)
////        .css({'fill': 'rgba(11, 104, 170,' + regions[i].population/highest_value +')'})
////        .data('region', regions[i]);
////    }
////
////    $('.map g').mouseover(function (e) {
////        var region_data=$(this).data('region');
////        $('<div class="info_panel">'+
////            region_data.region_name + '<br>' +
////          	'Population: ' + region_data.population.toLocaleString("en-UK") +
////          	'</div>'
////         )
////        .appendTo('body');
////    })
////    .mouseleave(function () {
////        $('.info_panel').remove();
////    })
////    .mousemove(function(e) {
////        var mouseX = e.pageX, //X coordinates of mouse
////            mouseY = e.pageY; //Y coordinates of mouse
////
////        $('.info_panel').css({
////            top: mouseY-50,
////            left: mouseX - ($('.info_panel').width()/2)
////        });
////    });
//
////    var coordMap = $('#svg-main')[0].getBoundingClientRect(),
////        coordWrapper = $('#inner')[0].getBoundingClientRect(),
////        coordInnerX = coordMap.x - coordWrapper.x
//
//
//
//
////    var positionMap = $('#inner').position();
//
////    var marginMap = positionMap.left/2;
////
////    $('#inner').css('margin-right', marginMap);
//
//    $(window).on('load scroll resize', function(){
//        var wrapperWidth = $('.map-wrapper').innerWidth();
//        var mapWidth = $('.map').innerWidth();
//        var corrector = (wrapperWidth - mapWidth)/2;
//        $('#map').attr('data-corrector', corrector);
//    }).trigger('scroll');
//
//
//
//
//
//
//    $('.appartments').mouseenter(function(e){
//        var $this = $(this);
////        var coordWrapper = $('#svg-main')[0].getBoundingClientRect();
////        var coordElement = $($this)[0].getBoundingClientRect();
////
////        var coordTooltipX = coordWrapper.x - coordElement.x   ;
////        var coordTooltipY = coordWrapper.y - coordElement.y   ;
//
//
//
//
////        var coord_elem = $this[0].getBoundingClientRect();
//
//
//
////        console.log( elem_height  );
//
//
//
//
//        if(!$this.hasClass('clicked')){
//            $this.attr('class', 'appartments selected');
//
//            var data = $this.attr('data-smalltext');
//            $('<div class="short_desc">'
//            + data
//            + '</div>').appendTo('#map');
//        }
////        var elem_height = $('.short_desc').innerHeight(),
////            elem_width = $('.short_desc').innerWidth();
//////        console.log(coord_elem.y - coordWrapper.y - elem_height - 20);
////        $('.short_desc').css({
////               top: coord_elem.y - coordWrapper.y - elem_height - 20,
////               left: coord_elem.x - coordWrapper.x + coord_elem.width/2 - elem_width/2
////            })
//
//
//
//
//        var mouseX = e.pageX, //X coordinates of mouse
//            mouseY = e.pageY; //Y coordinates of mouse
//        var corrector =  $('#map').attr('data-corrector');
//        console.log(corrector);
//        $('.short_desc').css({
//            top: mouseY-175,
//            left: mouseX - ($('.short_desc').width()/2) - corrector
//        });
//
////        console.log(positionMap);
////         console.log(marginMap);
//
//
//
////        console.log($this.hasClass('clicked'));
//
////        console.log(data);
//
//    }).mouseleave(function(){
//        $(this).attr('class', 'appartments');
//        $('.short_desc').remove();
//    }).on('click', function(e){
//        $('.appartments').removeClass('clicked');
//
//        $('.long_desc .include_info').appendTo('.include_info_wrapper');
//        $('.long_desc').remove();
//        var $thisElement = $(this);
//
////        var coord_elem = $thisElement[0].getBoundingClientRect();
//
////        console.log(coordWrapper);
////        console.log(coord_elem);
//
//        $thisElement.addClass('clicked');
//        $(this).attr('class', 'appartments');
//        $('.short_desc').remove();
//
//
////        var room_array= rooms.map( function( item ) {
////            return item.room_name;
////        });
////        console.log(room_array);
//
//
//
//        $('<div class="long_desc"><span class="close"></span></div>').appendTo('#map');
//
//        var id = $thisElement.attr('id');
////            include_info = $thisElement.parent().parent().parent('.svg-main').find('.include_info').attr('data-target', id)[0];
//            var include_info = $('.include_info[data-target='+ id +']');
////            console.log(include_info);
//            $(include_info).appendTo('.long_desc');
//
////        var elem_height = $('.long_desc').innerHeight(),
////            elem_width = $('.long_desc').innerWidth();
//////        console.log( elem_height  );
////        $('.long_desc').css({
////            top: coord_elem.y - coordWrapper.y - elem_height - 20,
////            left: coord_elem.x - coordWrapper.x + coord_elem.width/2 - elem_width/2
////        })
//
//        var mouseX = e.pageX, //X coordinates of mouse
//            mouseY = e.pageY; //Y coordinates of mouse
//
//        $('.long_desc').css({
//            top: mouseY-175,
//            left: mouseX - ($('.short_desc').width()/2 + 100)
//        });
//
////            console.log( "offX: " + offsetX + ", offY: " + offsetY );
////            console.log( "off: " + test  );
//        $('.close').click(function(){
//            $('.long_desc').remove();
//            $(include_info).appendTo('.include_info_wrapper');
//            $('.appartments').removeClass('clicked');
//        });
//    });
//    $('.zoomwrapper').each(function(i, el) {
//      let map = $('.zoomwrapper');
//
//      if (map.hasClass('is-scallable')) {
//
//        map.mousewheel(function(e){
//          e.preventDefault()
//          let matrix = map.css('transform')
//            .substr(7)
//            .replace(')', '')
//            .split(',')
//          let scale = +matrix[0] + e.deltaY * 0.1
//
////          console.log(scale)
//
//
//            $('.long_desc .include_info').appendTo('.include_info_wrapper');
//            $('.long_desc').remove();
//           $('.appartments').removeClass('clicked');
//            map.css('transform', ' scale(' + Math.min(Math.max(scale, 1), 10) + ')')
//            return false
//
//
//        })
//      }
//    });
//    $('.zoomwrapper').draggable({
//        start: function() {
//            $('.long_desc .include_info').appendTo('.include_info_wrapper');
//            $('.long_desc').remove();
//           $('.appartments').removeClass('clicked');
//        }
//    });
//});

//old


//$(document).ready(function () {
//    var coordWrapper = $('#svg-main')[0].getBoundingClientRect(),
//        zoomwrapper = $('.zoomwrapper');
////        zoomwrapperScale
//
//
//    $('.zoomwrapper').draggable();
//
////     $('.inner').mousemove(function(e) {
////        var x = e.offsetX==undefined?e.layerX:e.offsetX;
////        var y = e.offsetY==undefined?e.layerY:e.offsetY;
////
////        console.log( "pageX: " + x + ", pageY: " +  y );
////     });
//
//
//    $('.svg-main').mousemove(function(e) {
////        var coordWrapper = $('#map')[0].getBoundingClientRect();
////        var mouseX = e.pageX, // X coordinates of mouse
////            mouseY = e.pageY; // Y coordinates of mouse
////
////        var offsetX = e.offsetX,
////            offsetY = e.offsetY;
////
////
////        var dataZoom = Number(zoomwrapper.attr('data-zoom'));
//////            console.log( "pageX: " + mouseX + ", pageY: " + mouseY );
////            console.log( "offX: " + e.screenX  + ", offY: " + e.screenY  );
////        var thisElHeight = $('.short_desc').innerHeight();
////        var thisElWidth = $('.short_desc').innerWidth();
////         console.log( thisElHeight );
////        $('.short_desc').css({
//    //      top: mouseY-50,
//    //      left: mouseX - ($('.short_desc').width() / 2)
////            top: (offsetY * dataZoom) - thisElHeight - 20 ,
////            top: mouseY  ,
////            left: mouseX
////            left: (offsetX - thisElWidth/2) * dataZoom
////        });
////          console.log( "pos: " + coordWrapper.y + ", pageY: " + mouseY );
////        console.log( thisElHeight + ' ' + thisElWidth );
////        console.log( mouseY - coordWrapper.y );
//    });
//    $('.appartments').mouseenter(function(){
//        var $this = $(this);
////        var coordWrapper = $('#svg-main')[0].getBoundingClientRect();
////        var coordElement = $($this)[0].getBoundingClientRect();
////
////        var coordTooltipX = coordWrapper.x - coordElement.x   ;
////        var coordTooltipY = coordWrapper.y - coordElement.y   ;
//
//
//
//
//        var coord_elem = $this[0].getBoundingClientRect();
//
//
//
////        console.log( elem_height  );
//
//
//
//
//        if(!$this.hasClass('clicked')){
//            $this.addClass('selected');
//            var data = $this.attr('data-smalltext');
//            $('<div class="short_desc">'
//            + data
//            + '</div>').appendTo('.svg-main');
//        }
//        var elem_height = $('.short_desc').innerHeight(),
//            elem_width = $('.short_desc').innerWidth();
////        console.log(coord_elem.y - coordWrapper.y - elem_height - 20);
//        $('.short_desc').css({
//               top: coord_elem.y - coordWrapper.y - elem_height - 20,
//               left: coord_elem.x - coordWrapper.x + coord_elem.width/2 - elem_width/2
//            })
//
//
//
//
//
////        console.log($this.hasClass('clicked'));
//
////        console.log(data);
//
//    }).mouseleave(function(){
//        $(this).removeClass('selected');
//        $('.short_desc').remove();
//    }).on('click', function(){
//        $('.appartments').removeClass('clicked');
//
//        $('.long_desc .include_info').appendTo('.include_info_wrapper');
//        $('.long_desc').remove();
//        var $thisElement = $(this);
//
//        var coord_elem = $thisElement[0].getBoundingClientRect();
//
////        console.log(coordWrapper);
////        console.log(coord_elem);
//
//        $thisElement.addClass('clicked');
//        $('.appartments').removeClass('selected');
//        $('.short_desc').remove();
//
//
////        var room_array= rooms.map( function( item ) {
////            return item.room_name;
////        });
////        console.log(room_array);
//
//
//
//        $('<div class="long_desc"><span class="close"></span></div>').appendTo('.svg-main');
//
//        var id = $thisElement.attr('id');
////            include_info = $thisElement.parent().parent().parent('.svg-main').find('.include_info').attr('data-target', id)[0];
//            var include_info = $('.include_info[data-target='+ id +']');
////            console.log(include_info);
//            $(include_info).appendTo('.long_desc');
//
//        var elem_height = $('.long_desc').innerHeight(),
//            elem_width = $('.long_desc').innerWidth();
////        console.log( elem_height  );
//        $('.long_desc').css({
//            top: coord_elem.y - coordWrapper.y - elem_height - 20,
//            left: coord_elem.x - coordWrapper.x + coord_elem.width/2 - elem_width/2
//        })
////            console.log( "offX: " + offsetX + ", offY: " + offsetY );
////            console.log( "off: " + test  );
//        $('.close').click(function(){
//            $('.long_desc').remove();
//            $(include_info).appendTo('.include_info_wrapper');
//            $('.appartments').removeClass('clicked');
//        });
//    });
//
//
//
//    /*zzzzzzzzzzzzzzoooooooooooooommmmmmmm*/
//
//    var scaleMax = 3;
//
//
//    $('.svg-main>.zoomwrapper').each(function(i, el) {
//      let map = zoomwrapper;
//
//      if (map.hasClass('is-scallable')) {
//
//        map.mousewheel(function(e){
//          e.preventDefault()
//          let matrix = map.css('transform')
//            .substr(7)
//            .replace(')', '')
//            .split(',')
//          let scale = +matrix[0] + e.deltaY * 0.1
//
////          console.log(scale)
//
//           $('.long_desc .include_info').appendTo('.include_info_wrapper');
//            $('.long_desc').remove();
//           $('.appartments').removeClass('clicked');
//            if( scale <= scaleMax ){
//                map.css('transform', ' scale(' + Math.min(Math.max(scale, 1), 10) + ')')
//                return false
//            }
//
//        })
//      }
//    });
//
//    $(zoomwrapper).mousewheel(function(){
//        var matrixxx = zoomwrapper.css('transform')
//        .substr(7).replace(')', '').split(',');
////            return matrixxx[0];
////           console.log('ddd ' + matrixxx[0]);
//        zoomwrapper.attr('data-zoom', matrixxx[0]);
//    })
//
//    $('.zoom-buttons').on('click', '.zoomin-button', function(){
//        var dataZoom = Number(zoomwrapper.attr('data-zoom'));
//        $(this).parent().find($('.zoomout-button')).removeClass('btn-disabled');
//
//        if(dataZoom < scaleMax ){
//            zoomwrapper.attr('data-zoom', dataZoom + 0.5);
//            zoomwrapper.css('transform', 'translate(-50%, -50%) scale(' + (dataZoom + 0.5) + ')')
//        } else{
//            $(this).addClass('btn-disabled');
//        }
//    }).on('click', '.zoomout-button', function(){
//        var dataZoom = Number(zoomwrapper.attr('data-zoom'));
//        $(this).parent().find($('.zoomin-button')).removeClass('btn-disabled');
//        if(dataZoom >= 1.1 ){
//            zoomwrapper.attr('data-zoom', dataZoom - 0.5);
//            zoomwrapper.css('transform', 'translate(-50%, -50%) scale(' + (dataZoom - 0.5) + ')')
//        }else{
//            $(this).addClass('btn-disabled');
//        }
//    });
//
//
//
//
//
//});
//
//
//
//
"use strict";
