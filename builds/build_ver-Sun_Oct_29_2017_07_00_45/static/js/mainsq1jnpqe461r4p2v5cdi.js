'use strict';

$(document).ready(function () {
    var coordWrapper = $('#svg-main')[0].getBoundingClientRect(),
        zoomwrapper = $('.zoomwrapper');
    //        zoomwrapperScale 


    $('.svg-main').draggable();

    $('.svg-main').mousemove(function (e) {
        var coordWrapper = $('#svg-main')[0].getBoundingClientRect();
        var mouseX = e.pageX,
            // X coordinates of mouse
        mouseY = e.pageY; // Y coordinates of mouse

        var offsetX = e.offsetX,
            offsetY = e.offsetY;

        //            console.log( "pageX: " + mouseX + ", pageY: " + mouseY );
        //            console.log( "offX: " + offsetX + ", offY: " + offsetY );
        var thisElHeight = $('.short_desc').innerHeight();
        var thisElWidth = $('.short_desc').innerWidth();
        $('.short_desc').css({
            //      top: mouseY-50,
            //      left: mouseX - ($('.short_desc').width() / 2)  
            top: mouseY - coordWrapper.y - thisElHeight - 20,
            left: mouseX - coordWrapper.x - thisElWidth / 2
        });
        //          console.log( "pos: " + coordWrapper.y + ", pageY: " + mouseY );
        //        console.log( thisElHeight + ' ' + thisElWidth );
        //        console.log( mouseY - coordWrapper.y );
    });
    $('.appartments').mouseenter(function () {
        var $this = $(this);
        if (!$this.hasClass('clicked')) {
            $this.addClass('selected');
            var data = $this.attr('data-smalltext');
            $('<div class="short_desc">' + data + '</div>').appendTo('.svg-main');
        }

        //        console.log($this.hasClass('clicked'));

        //        console.log(data);
    }).mouseleave(function () {
        $(this).removeClass('selected');
        $('.short_desc').remove();
    }).on('click', function () {
        $('.appartments').removeClass('clicked');
        $('.long_desc').remove();

        var $thisElement = $(this);

        var coord_elem = $thisElement[0].getBoundingClientRect();

        //        console.log(coordWrapper);
        //        console.log(coord_elem);

        $thisElement.addClass('clicked');
        $('.appartments').removeClass('selected');
        $('.short_desc').remove();

        $('<div class="long_desc"><span class="close">x</span></div>').appendTo('.svg-main');

        var id = $thisElement.attr('id'),
            include_info = $thisElement.parent().parent().parent('.svg-main').find('.include_info').attr('data-target', id)[0];
        //            console.log(include_info);
        $(include_info).appendTo('.long_desc');

        var elem_height = $('.long_desc').innerHeight(),
            elem_width = $('.long_desc').innerWidth();
        //        console.log( elem_height  );
        $('.long_desc').css({
            top: coord_elem.y - coordWrapper.y - elem_height - 20,
            left: coord_elem.x - coordWrapper.x + coord_elem.width / 2 - elem_width / 2
        });
        //            console.log( "offX: " + offsetX + ", offY: " + offsetY );
        //            console.log( "off: " + test  );
        $('.close').click(function () {
            $('.long_desc').remove();
            $(include_info).appendTo('.include_info_wrapper');
            $('.appartments').removeClass('clicked');
        });
    });

    /*zzzzzzzzzzzzzzoooooooooooooommmmmmmm*/

    var scaleMax = 3;

    $('.svg-main>.zoomwrapper').each(function (i, el) {
        var map = zoomwrapper;

        if (map.hasClass('is-scallable')) {

            map.mousewheel(function (e) {
                e.preventDefault();
                var matrix = map.css('transform').substr(7).replace(')', '').split(',');
                var scale = +matrix[0] + e.deltaY * 0.1;

                //          console.log(scale)
                $('.long_desc').remove();
                $('.long_desc .include_info').appendTo('.include_info_wrapper');
                $('.appartments').removeClass('clicked');
                if (scale <= scaleMax) {
                    map.css('transform', 'translate(-50%, -50%) scale(' + Math.min(Math.max(scale, 1), 10) + ')');
                    return false;
                }
            });
        }
    });

    $(zoomwrapper).mousewheel(function () {
        var matrixxx = zoomwrapper.css('transform').substr(7).replace(')', '').split(',');
        //            return matrixxx[0];
        //           console.log('ddd ' + matrixxx[0]); 
        zoomwrapper.attr('data-zoom', matrixxx[0]);
    });

    $('.zoom-buttons').on('click', '.zoomin-button', function () {
        var dataZoom = Number(zoomwrapper.attr('data-zoom'));
        $(this).parent().find($('.zoomout-button')).removeClass('btn-disabled');

        if (dataZoom < scaleMax) {
            zoomwrapper.attr('data-zoom', dataZoom + 0.5);
            zoomwrapper.css('transform', 'translate(-50%, -50%) scale(' + (dataZoom + 0.5) + ')');
        } else {
            $(this).addClass('btn-disabled');
        }
    }).on('click', '.zoomout-button', function () {
        var dataZoom = Number(zoomwrapper.attr('data-zoom'));
        $(this).parent().find($('.zoomin-button')).removeClass('btn-disabled');
        if (dataZoom >= 1.1) {
            zoomwrapper.attr('data-zoom', dataZoom - 0.5);
            zoomwrapper.css('transform', 'translate(-50%, -50%) scale(' + (dataZoom - 0.5) + ')');
        } else {
            $(this).addClass('btn-disabled');
        }
    });
});
"use strict";