$(function() {

    $(document).ready(function(){
        var target = $('.include_info_wrapper .active').attr('data-target');
//        console.log(target)
        $('#' + target).addClass('clicked');

    })
    
    $('.appartments').mouseover(function (e) {
        var $this = $(this);
        if(!$this.hasClass('clicked')){
            $this.addClass('selected'); 
            var data = $this.attr('data-smalltext');
//            console.log(data);
            $('<div class="short_desc">'
            + data
            + '</div>').appendTo('#svg-main');
//            var elem_height = $('.short_desc').innerHeight(),
//                elem_width = $('.short_desc').innerWidth();
            
        }
    })
    .mouseleave(function () {
        $(this).removeClass('selected'); 
        $('.short_desc').remove();
    })
    .mousemove(function(e) {
        var mouseX = e.pageX, //X coordinates of mouse
            mouseY = e.pageY, //Y coordinates of mouse
            coord = $(this)[0].getBoundingClientRect(),
            coord2 = $('.svg-plan')[0].getBoundingClientRect(),
            resultCoord = coord.x - coord2.x;
//        console.log(mouseX + ' ' + mouseY)
//        console.log(resultCoord)
        $('.short_desc').css({
            top: mouseY-100,
//            left: mouseX - ($('.short_desc').width()/2) - 1140
            left: resultCoord - 130
        });
    }).on('click', function(e){
        $('.appartments').removeClass('clicked');
        
        $('.include_info_wrapper .include_info').removeClass('active');

        var $thisElement = $(this);
        $thisElement.addClass('clicked');
        $('.short_desc').remove();
            
        var id = $thisElement.attr('id');
        var include_info = $('.include_info[data-target='+ id +']');
        $(include_info).addClass('active');
        
//        var heightMT = $('.include_info.active .room-info').innerHeight;
////            doc_w = $(document).width();
//        if(window.innerWidth > 992){
//            console.log(heightMT);
//            $('.house_trigger .inner').css('margin-top', -heightMT);
//        }
    });
    
    
    
    $(document).ready(function(){
        var target2 = $('.include_info_wrapper2 .active').attr('data-target');
//        console.log(target2)
        $('#' + target2).addClass('clicked');
    })
    
    $('.appartments2').mouseover(function (e) {
        var $this2 = $(this);
        if(!$this2.hasClass('clicked')){
            $this2.addClass('selected'); 
            var data2 = $this2.attr('data-smalltext2');
//            console.log(data);
            $('<div class="short_desc">'
            + data2
            + '</div>').appendTo('#svg-main2');
//            var elem_height = $('.short_desc').innerHeight(),
//                elem_width = $('.short_desc').innerWidth();
            
        }
    })
    .mouseleave(function () {
        $(this).removeClass('selected'); 
        $('.short_desc').remove();
    })
    .mousemove(function(e) {
        var mouseX = e.pageX, //X coordinates of mouse
            mouseY = e.pageY, //Y coordinates of mouse
            coord = $(this)[0].getBoundingClientRect(),
            coord2 = $('.svg-plan2')[0].getBoundingClientRect(),
            resultCoord = coord.x - coord2.x;
//        console.log(mouseX + ' ' + mouseY)
//        console.log(resultCoord)
        $('.short_desc').css({
            top: mouseY-100,
//            left: mouseX - ($('.short_desc').width()/2) - 1140
            left: resultCoord - 130
//            left: mouseX - coord2
        });
    }).on('click', function(e){
        $('.appartments2').removeClass('clicked');
        
        $('.include_info_wrapper2 .include_info').removeClass('active');

        var $thisElement = $(this);
        $thisElement.addClass('clicked');
        $('.short_desc').remove();
            
        var id = $thisElement.attr('id');
        var include_info = $('.include_info_wrapper2  .include_info[data-target='+ id +']');
        $(include_info).addClass('active');
        
    });
    
    
//    $('.section-trigger1 .check').click(function(){
//        $('.section-trigger1 .check').removeClass('checked');
//        $(this).addClass('checked');
//        var targetSection =  $(this).attr('data-sectiontarget'),
//            section = $('#bw1  [data-section='+ targetSection +']');
//        $('.build1 .section').removeClass('vis');
////        console.log(targetSection);
////        console.log(section );
//        $(section).addClass('vis');
//    });
//
//    $('.section-trigger2 .check').click(function(){
//        $('.section-trigger2 .check').removeClass('checked');
//        $(this).addClass('checked');
//        var targetSection =  $(this).attr('data-sectiontarget'),
//            section = $('#bw2 [data-section='+ targetSection +']');
//        $('.build2 .section').removeClass('vis');
////        console.log(targetSection);
////        console.log(section );
//        $(section).addClass('vis');
//    });
//      $('.house_number:not(.disable)').click(function(){
//        $('[house_wrapper]').removeClass('active');
//
//
////        $(this).addClass('active');
//        var nowHouse= $(this).attr('data-number');
//        var activeHouse = $('[house_wrapper='+ nowHouse +']');
//        $(activeHouse).addClass('active');
//
////        console.log(activeHouse);
//    });

    //house & section trigger

    $('.section-trigger .porch').click(function(){
        $('.section-trigger .porch').removeClass('active');
        $(this).addClass('active');

        var targetSection =  $(this).attr('data-sectiontarget');

        if(targetSection > 3){
            //trigger plansection block
            $('.houses').removeClass('active');
            $('.houses[house_wrapper="house2"]').addClass('active');


            //trigger this clickable svg
            var section = $('#bw2  [data-section='+ targetSection +']');
            $('.build2 .section').removeClass('vis');
            $(section).addClass('vis');


            //trigger description svg
            $('.plans-wrapper').removeClass('active');
            $('.plans-wrapper[house_wrapper="house2"]').addClass('active');


//            console.log(section)
        }else{
            //trigger plansection block
            $('.houses').removeClass('active');
            $('.houses[house_wrapper="house1"]').addClass('active');


            //trigger this clickable svg
            var section = $('#bw1  [data-section='+ targetSection +']');
            $('.build1 .section').removeClass('vis');
            $(section).addClass('vis');


            //trigger description svg
            $('.plans-wrapper').removeClass('active');
            $('.plans-wrapper[house_wrapper="house1"]').addClass('active');


        }

//
//
//
//
//        console.log(targetSection);
//        console.log(section );
//        $(section).addClass('vis');
    });
});
